# Generated by Django 4.1 on 2022-08-25 00:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0005_alter_step_order"),
        ("tags", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Step",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "food_items",
                    models.ManyToManyField(blank=True, to="recipes.fooditem"),
                ),
            ],
        ),
    ]
